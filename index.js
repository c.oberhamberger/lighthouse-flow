import fs from 'fs';
import path from 'path';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse';

import { waitForSelectors } from './helper/index.js';

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const session = await page.target().createCDPSession();
    const testUrl = 'https://www.melectronics.ch/de?hide=gtm,rating,cookie,newsletter';
    const flow = await startFlow(page, {name: 'MEL add to cart', });

    {
        console.log('Cold Navigation');
        await flow.navigate(testUrl, {
          stepName: 'Testing Cold navigation'
        });
    }
    {
       console.log('Warm Navigation');
        await flow.navigate(testUrl, {
          stepName: 'Warm navigation'
        });
    }
    {
        console.log('Testing Search for iphone');
        const targetPage = page;
        await flow.startTimespan({stepName: 'search for iphone'});
        const element = await waitForSelectors([["aria/Suchen[role=\"textbox\"]"],["#test-header-input-search"]], targetPage);
        await element.click({ offset: { x: 115.5, y: 25.59375} });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["aria/Suchen[role=\"textbox\"]"],["#test-header-input-search"]], targetPage);
        const type = await element.evaluate(el => el.type);
        if (["textarea","select-one","text","url","tel","search","password","number","email"].includes(type)) {
          await element.type('iphone');
        } else {
          await element.focus();
          await element.evaluate((el, value) => {
            el.value = value;
            el.dispatchEvent(new Event('input', { bubbles: true }));
            el.dispatchEvent(new Event('change', { bubbles: true }));
          }, "iphone");
        }
    }
    {
        const targetPage = page;
        await targetPage.keyboard.down("Enter");
    }
    {
        const targetPage = page;
        await targetPage.keyboard.up("Enter");
    }
    {
        const targetPage = page;
        await targetPage.evaluate((x, y) => { window.scroll(x, y); }, 0, 228)
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["#test-tile-0 > a"]], targetPage);
        await flow.endTimespan();

        console.log('Testing iphone results');
        await flow.snapshot({stepName: 'PLP Searchresults for iphones'});

        console.log('Testing click on iphone');
        await flow.startTimespan({stepName: 'click on iphone'});
        await element.click({ offset: { x: 34.65625, y: 61.3984375} });
    }
    {
        const targetPage = page;
        await targetPage.evaluate((x, y) => { window.scroll(x, y); }, 0, 271)

        await flow.endTimespan();
        console.log('Testing Iphone PDP');

        await flow.snapshot({stepName: 'iPhone PDP'});

    }
    {
        const targetPage = page;

        const element = await waitForSelectors([["#test-detail-btn-order"]], targetPage);
        console.log('Testing click on add-to-cart');

        await flow.startTimespan({stepName: 'click on add-to-cart'});
        await element.click({ offset: { x: 158, y: 20.921875} });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["#test-lightbox-btn-add"]], targetPage);
        await flow.endTimespan();
        console.log('Testing add to cart in lightbox');

        await flow.snapshot({stepName: 'Services Overlay'});
        await flow.startTimespan({stepName: 'click on add-to-cart in service lightbox'});

        await element.click();
    }
     {
        const targetPage = page;

        const element = await waitForSelectors([[".micro-cart--icon"]], targetPage);
        await flow.endTimespan();
        console.log('Testing getting to Cart page');
        await flow.startTimespan({stepName: 'get to cart page'});

        await element.click();
    }
    {
        const targetPage = page;
        await targetPage.evaluate((x, y) => { window.scroll(x, y); }, 0, 0)
        await flow.endTimespan();
        console.log('Testing Cart page');
        await flow.snapshot({stepName: 'Cart Page'});
    }

    await browser.close();

    const jsonResult = await flow.createFlowResult();
    const report = await flow.generateReport();


    const distDir = path.join(path.resolve() + '/dist/');  
    if (!fs.existsSync('dist')) {
      fs.mkdirSync('dist')
    }
    fs.writeFileSync(distDir + 'flow.report.json', JSON.stringify(jsonResult));
    fs.writeFileSync(distDir + 'flow.report.html', report);

    open('./dist/flow.report.html', {wait: false});
})();
